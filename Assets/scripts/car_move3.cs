﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class car_move3 : MonoBehaviour {

    public float acceleration;
    public float steering;
    private Rigidbody2D rb;
    public float driftStart;
    public float maxSpeed;
    private float startMxSpeed;
    private float startDriftStart;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {

        float h = 0;
        float v = 0;
        //float h = -Input.GetAxis("Horizontal");

        //float v = Input.GetAxis("Vertical");


        if (Input.GetKey(KeyCode.K))
        {
            h = 1;
        }
        else if (Input.GetKey(KeyCode.H))
        {
            h = -1;
        }
        else
        {
            h = 0;
        }

        if (Input.GetKey(KeyCode.U))
        {
            v = -1;
        }
        else if (Input.GetKey(KeyCode.J))
        {
            v = 1;
        }
        else
        {
            v = 0;
        }



        Vector2 speed = transform.up * (v * acceleration);
        rb.AddForce(speed);

        float direction = Vector2.Dot(rb.velocity, rb.GetRelativeVector(Vector2.up));
        if (direction >= -0.0f)
        {
            rb.rotation += h * steering * (rb.velocity.magnitude / 5.0f);
            //rb.AddTorque((h * steering) * (rb.velocity.magnitude / 10.0f));
        }
        else
        {
            rb.rotation -= h * steering * (rb.velocity.magnitude / 5.0f);
            // rb.AddTorque((-h * steering) * (rb.velocity.magnitude / 10.0f));
        }

        Vector2 forward = new Vector2(0.0f, 0.5f);
        float steeringRightAngle;
        if (rb.angularVelocity > 0)
        {
            steeringRightAngle = -90;
        }
        else
        {
            steeringRightAngle = 90;
        }
        //add opposite force (drift)
        rb.AddForce(-transform.up * (rb.velocity.magnitude / 2) * driftStart * Input.GetAxis("Horizontal"));
        //decrease drifting multiplier 
        if (driftStart > 0.2f)
        {
            driftStart -= 0.01f;
        }
        //decrease general speed 
        if (maxSpeed < 10)
        {
            maxSpeed -= 0.5f;
        }
        else
        {
            //reset 
            driftStart = startDriftStart;

            maxSpeed = startMxSpeed;

        }


        Vector2 rightAngleFromForward = Quaternion.AngleAxis(steeringRightAngle, Vector3.forward) * forward;
        Debug.DrawLine((Vector3)rb.position, (Vector3)rb.GetRelativePoint(rightAngleFromForward), Color.green);

        float driftForce = Vector2.Dot(rb.velocity, rb.GetRelativeVector(rightAngleFromForward.normalized));

        Vector2 relativeForce = (rightAngleFromForward.normalized * -1.0f) * (driftForce * 10.0f);


        Debug.DrawLine((Vector3)rb.position, (Vector3)rb.GetRelativePoint(relativeForce), Color.red);

        rb.AddForce(rb.GetRelativeVector(relativeForce));
    }
}
