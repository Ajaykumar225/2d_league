﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class score : MonoBehaviour
{
    public static int score_red;
    public static int score_blue;
    public Text Score_red;
    public Text Score_blue;
    public static Transform car1;
    public Transform car2;
    public Transform car3;
    public Transform car4;
    public Rigidbody2D rr;

    // Use this for initialization
    void Start()
    {

        score_red = 0;

        score_blue = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Score_red.text = "RED:" + score_red;
        Score_blue.text = "BLUE:" + score_blue;
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "red")
        {

            this.transform.position = new Vector3(0, 0, 0);

            rr.velocity = Vector3.zero;
            car1.transform.position = new Vector3(-32.2f, 2.4f, 0);
            car2.transform.position = new Vector3(32.9f, -1.1f, 0);
            car1.transform.eulerAngles = new Vector3(0, 0, 90);
            car2.transform.eulerAngles = new Vector3(0, 0, -90);
            //     car3.transform.position = new Vector3(32.9f, -1.1f, 0);
            //  car4.transform.position = new Vector3(33.0f, 3.6f, 0);
            // Destroy(this.gameObject);
            //Instantiate(Resources.Load("Ball"),new Vector2(14.2f, -22.2f), Quaternion.identity);
        }
        if (collision.gameObject.tag == "blue")
        {
            this.transform.position = new Vector3(0, 0, 0);
            rr.velocity = Vector3.zero;
            car1.transform.position = new Vector3(-32.2f, 2.4f, 0);
            car2.transform.position = new Vector3(32.9f, -1.1f, 0);
            car1.transform.eulerAngles = new Vector3(0, 0, 90);
            car2.transform.eulerAngles = new Vector3(0, 0, -90);
            //   car3.transform.position = new Vector3(32.9f, -1.1f, 0);
            // car4.transform.position = new Vector3(33.0f, 3.6f, 0);
            //Destroy(this.gameObject);
            //Instantiate(Resources.Load("Ball"), new Vector2(14.2f, -22.2f), Quaternion.identity);
        }
    }
}

  

