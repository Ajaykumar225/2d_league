﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class carmove : NetworkBehaviour
{/*
    public Rigidbody2D rb;
    public float Speed;
    public float Steer;
   public int Invert;
    // Update is called once per frame
    public void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            Invert = 1;
            rb.AddForce(Invert*(transform.up) * Speed * Time.deltaTime);
            
        }
        if (Input.GetKey(KeyCode.S))
        {
            Invert = -1;
            rb.AddForce(Invert * (transform.up) * Speed * Time.deltaTime);

        }


        if (Input.GetKey(KeyCode.A))
            transform.Rotate(0f, 0, Invert * Steer);
        if (Input.GetKey(KeyCode.D))
            transform.Rotate(0f, 0, Invert * -Steer);


    }
    public void ajay()
    {
        Invert = -1;
    }
    public void aj()
    {
        {
            
            Invert = 1;
        }
    }*/
    public float acceleration;
    public float steering;
    private Rigidbody2D rb;
    public float driftStart;
    public float maxSpeed;
    private float startMxSpeed;
    private float startDriftStart;
   

    private int windSpeed=10;
    public GameObject car1;
   
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
   public void Update()
    {
     if(!isLocalPlayer)
        {
            
            return;
        }
    }

    void FixedUpdate()
    {
        

        float h = 0;
        float v = 0;
        //float h = -Input.GetAxis("Horizontal");

        //float v = Input.GetAxis("Vertical");

      
        {
           // GetComponent<Rigidbody2D>().AddForce(-new Vector3(2,5,0) * windSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            h = 1;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            h = -1;
        }
        else
        {
            h = 0;
        }

        if (Input.GetKey(KeyCode.W))
        {
            v = -1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            v = 1;
        }
        else
        {
            v = 0;
        }
        
       
        Vector2 speed = transform.up * (v * acceleration);
        rb.AddForce(speed);

        float direction = Vector2.Dot(rb.velocity, rb.GetRelativeVector(Vector2.up));
        if (direction >= -0.0f)
        {
             rb.rotation += h * steering * (rb.velocity.magnitude / 5.0f);
            //rb.AddTorque((h * steering) * (rb.velocity.magnitude / 10.0f));
        }
        else
        {
            rb.rotation -= h * steering * (rb.velocity.magnitude / 5.0f);
           // rb.AddTorque((-h * steering) * (rb.velocity.magnitude / 10.0f));
        }

        Vector2 forward = new Vector2(0.0f, 0.5f);
        float steeringRightAngle;
        if (rb.angularVelocity > 0)
        {
            steeringRightAngle = -90;
        }
        else
        {
            steeringRightAngle = 90;
        }
        //add opposite force (drift)
       rb.AddForce(-transform.up * (rb.velocity.magnitude / 2) * driftStart * Input.GetAxis("Horizontal"));
        //decrease drifting multiplier 
        if (driftStart > 0.2f)
        {
            driftStart -= 0.01f;
        }
        //decrease general speed 
        if (maxSpeed < 10)
        {
            maxSpeed -= 0.5f;
        }
        else
        {
            //reset 
            driftStart = startDriftStart;
           
            maxSpeed = startMxSpeed;

        }


        Vector2 rightAngleFromForward = Quaternion.AngleAxis(steeringRightAngle, Vector3.forward) * forward;
        Debug.DrawLine((Vector3)rb.position, (Vector3)rb.GetRelativePoint(rightAngleFromForward), Color.green);

        float driftForce = Vector2.Dot(rb.velocity, rb.GetRelativeVector(rightAngleFromForward.normalized));

        Vector2 relativeForce = (rightAngleFromForward.normalized * -1.0f) * (driftForce * 10.0f);


        Debug.DrawLine((Vector3)rb.position, (Vector3)rb.GetRelativePoint(relativeForce), Color.red);

        rb.AddForce(rb.GetRelativeVector(relativeForce));
    }
    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
    }

}
    /*public Rigidbody2D rb;
    
    private float moveForce;
    private float maxSpeed;
    private float rotationSpeed;
    private float driftStart;
    private float startDriftStart;
    private float startRotation;
    private float startMxSpeed;
    private float acceleration;

    public int Speed;
    public int Invert;
    void FixedUpdate()
    {
        //move car "forward"
        float v = Input.GetAxis("Vertical");
        Vector2 speed = transform.up * (v * acceleration);
        rb.AddForce(speed);
        if (Input.GetKey(KeyCode.W))
        {
            Invert = 1;
            rb.AddForce( (transform.up) * Speed * Time.deltaTime);

        }
        if (Input.GetKey(KeyCode.S))
        {
            Invert = -1;
            rb.AddForce(- (transform.up) * Speed * Time.deltaTime);

        }
        //limit car velocity    
       // rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxSpeed);

        //turning the car 
        if (Mathf.Abs(Invert*Input.GetAxis("Horizontal")) > 0)
        {
            rb.angularVelocity =Invert* Input.GetAxis("Horizontal") * -rotationSpeed;
            //increase rotation force 
            if (rotationSpeed < 300)
            {
                rotationSpeed += Time.deltaTime * 20;
            }
            //add opposite force (drift)
            rb.AddForce(transform.up * (rb.velocity.magnitude / 2) *driftStart*Invert * Input.GetAxis("Horizontal"));
            //decrease drifting multiplier 
            if (driftStart > 0.2f)
            {
                driftStart -= 0.01f;
            }
            //decrease general speed 
            if (maxSpeed < 10)
            {
                maxSpeed -= 0.5f;
            }
        }
        else
        {
            //reset 
            driftStart = startDriftStart;
            rotationSpeed = startRotation;
            maxSpeed = startMxSpeed;

        }

    }  


}
*/